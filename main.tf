terraform {
  backend "s3" {
    region  = "us-east-1"
    bucket  = "terraform-state-bucket-jeffin-joseph"
    key     = "global/task2.tfstate"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  # Configuration options
  region = "us-east-1"
}


