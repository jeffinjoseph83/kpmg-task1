3 Tier Architecture in AWS using Terraform

AWS documentation reference- [Aws docs](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-lamp-amazon-linux-2.html)


Link to Terraform modules below

Terraform VPC module - [link](https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/1.23.0)

Terraform ALB module - [link](https://registry.terraform.io/modules/terraform-aws-modules/alb/aws/latest)

Terraform Security group modules - [link](https://registry.terraform.io/modules/terraform-aws-modules/security-group/aws/3.0.1/submodules/http-80), [link](https://registry.terraform.io/modules/terraform-aws-modules/security-group/aws/3.0.1)

Terraform ASG module - [link](https://registry.terraform.io/modules/terraform-aws-modules/autoscaling/aws/latest)

Terraform RDS module - [link](https://registry.terraform.io/modules/terraform-aws-modules/security-group/aws/3.0.1)






